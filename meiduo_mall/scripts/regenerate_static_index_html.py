

# 从 scripts 往上找一个目录，就是 meiduo_mall
import sys
sys.path.insert(0, '../')


# 导入django的配置
import os
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'meiduo_mall.settings.dev'

# 让django进行初始化
import django
django.setup()

from contents.crons import generate_static_index_html

""" 运行静态化脚本时需要依赖django的环境 """

if __name__ == '__main__':
    generate_static_index_html()