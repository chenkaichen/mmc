import re
from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from goods.models import SKU
from users.models import User, Address
from celery_tasks.email.tasks import send_active_email
from . import constants


class SKUSerializer(serializers.ModelSerializer):
    """ 返回商品SKU信息序列化器 """
    class Meta:
        model = SKU
        fields = ("id", "name", "price", "default_image_url", "comments")


class AddUserBrowsingHistorySerializer(serializers.Serializer):
    """ 添加用户浏览历史序列化器 """
    sku_id = serializers.IntegerField(label="商品SKU编号", min_value=1)

    def validate_sku_id(self, attrs):
        """ 检验 sku_id 是否存在 """
        try:
            SKU.objects.get(id=attrs)
        except SKU.DoesNotExist():
            raise serializers.ValidationError("该商品不存在")
        return attrs

    def create(self, validated_data):
        """ 保存 """
        user_id = self.context["request"].user.id
        sku_id = validated_data["sku_id"]

        redis_conn = get_redis_connection("history")
        pl = redis_conn.pipeline()

        # 移除已经存在的本商品的浏览记录
        pl.lrem(f"history_{user_id}", 0, sku_id)
        # 添加新的浏览记录(lpush: 从左边开始添加)
        pl.lpush(f"history_{user_id}", sku_id)
        # 最多保留5条数据(截断，从index索引开始计算)
        pl.ltrim(f"history_{user_id}", 0, constants.USER_BROWSE_HISTORY_MAX_LIMIT - 1)
        pl.execute()
        return validated_data


class AddressTitleSerializer(serializers.ModelSerializer):
    """ 修改收货地址标题-序列化器 """
    class Meta:
        model = Address
        fields = ('title',)


class UserAddressSerializer(serializers.ModelSerializer):
    """ 收货地址-序列化器"""
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)
    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)

    class Meta:
        model = Address
        exclude = ('user', 'is_deleted', 'create_time', 'update_time')

    def validate_mobile(self, value):
        """ 验证手机号 """
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def create(self, validated_data):
        """ 保存 """
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class EmailSerializer(serializers.ModelSerializer):
    """ 邮箱-序列化器 """
    class Meta:
        model = User
        fields = ('id', 'email')
        extra_kwargs = {
            'email': {
                'required': True
            }
        }

    def update(self, instance, validated_data):
        """
        :param instance: 视图传过来的 user 对象
        :param validated_data:
        :return:
        """
        instance.email = validated_data['email']
        # 保存用户的email地址，发送发送激活链接时需要用到
        instance.save()

        # 生成激活链接
        verify_url = instance.generate_verify_email_url()
        # 使用 celery 发送邮件
        send_active_email.delay(instance.email, verify_url)

        return instance


# GET /user/
class UserDetailSerializer(serializers.ModelSerializer):
    """ 用户基本信息-序列化器 """
    class Meta:
        model = User
        fields =('id', 'username', 'mobile', 'email', 'email_active')


# 需要序列化的数据大多从模型类中获取，所以继承ModelSerializer
class CreateUserSerializer(serializers.ModelSerializer):
    """创建用户的序列化器"""

    password2 = serializers.CharField(label='确认密码', write_only=True)
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)
    # 序列化时颁发签名
    token = serializers.CharField(label='JWT token', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'password2', 'sms_code', 'mobile', 'allow', 'token')
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    def validate_mobile(self, value):
        """验证手机号"""
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def validate_allow(self, value):
        """检验用户是否同意协议"""
        if value != 'true':
            raise serializers.ValidationError('请同意用户协议')
        return value

    def validate(self, data):
        # 判断两次密码
        if data['password'] != data['password2']:
            raise serializers.ValidationError('两次密码不一致')

        # 判断短信验证码
        try:
            redis_conn = get_redis_connection('verify_codes')
            mobile = data['mobile']
            real_sms_code = redis_conn.get('sms_%s' % mobile)
        except Exception as e:
            raise serializers.ValidationError(f"[异常:{e}]")
        if real_sms_code is None:
            raise serializers.ValidationError('无效的短信验证码')
        if data['sms_code'] != real_sms_code.decode():
            raise serializers.ValidationError('短信验证码错误')
        return data

    def create(self, validated_data):
        """重写保存方法"""

        # del validated_data['password2']
        # del validated_data['sms_code']
        # del validated_data['allow']

        # 移除数据库模型类中的不存在的属性
        validated_data.pop('password2', None)
        validated_data.pop('sms_code', None)
        validated_data.pop('allow', None)

        # user = User.objects.create(username=xxx, password=xxx)
        # user = User.objects.create(**validated_data)
        user = super().create(validated_data)

        # 调用django的认证系统对密码进行加密
        user.set_password(validated_data['password'])
        user.save()

        # 手动颁发签名
        jwt_payload_hendler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_hendler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_hendler(user)
        token = jwt_encode_hendler(payload)

        # 返回之前给用户对象添加token属性
        user.token = token

        # 在 context 中添加user属性，用于注册后合并用户的购物车
        self.context["request"].user = user

        return user