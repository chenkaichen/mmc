from django.contrib.auth.backends import ModelBackend
from django.db.models import Q
from users.models import User
import logging

logger = logging.getLogger('django')


def jwt_response_payload_handler(token, user=None, request=None):
    """
    自定义jwt认证成功返回数据
    """
    return {
        'token': token,
        'user_id': user.id,
        'username': user.username
    }


class UsernameMobileAuthBackend(ModelBackend):
    """
    自定义用户名或手机号认证
    """
    def authenticate(self, request, username=None, password=None, **kwargs):
        """username可能是用户名，也可能是手机号"""
        # 获取用户对象(使用Q对象来处理)
        try:
            user = User.objects.filter(Q(username=username) | Q(mobile=username)).get()
        except Exception as e:
            logger.error(f"数据库查询异常:[message: {e}]")
            return
        # 如果用户存在，校验密码
        if user and user.check_password(password):

            return user
        # 如果不存在，默认返回None
