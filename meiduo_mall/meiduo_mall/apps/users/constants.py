

# 设置邮件验证url链接的有效期，单位秒
VERIFY_EMAIL_TOKEN_EXPIRES = 24 *  60 * 60

# 用户地址列表单页最大显示数量
USER_ADDRESS_COUNTS_LIMIT = 10

# 用户浏览历史记录最大保存数量
USER_BROWSE_HISTORY_MAX_LIMIT = 5