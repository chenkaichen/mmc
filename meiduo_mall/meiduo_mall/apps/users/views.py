from django.shortcuts import render
import logging
from django.utils.decorators import method_decorator
from django_redis import get_redis_connection
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status, mixins
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework_jwt.views import ObtainJSONWebToken

from carts.utils import merge_cart_decoration
from goods.models import SKU
from users import serializers, constants
from users.models import User
from users.serializers import CreateUserSerializer


logger = logging.getLogger("django")


@method_decorator(merge_cart_decoration, name="post")
class MyObtainJSONWebToken(ObtainJSONWebToken):
    """ 用户登录认证视图 """
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)

        # 如果用户登录成功，合并购物车
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data["user"]
            request.user = user
        return response


class UserBrowsingHistoryView(CreateAPIView):
    """ 用户浏览历史记录 """
    serializer_class = serializers.AddUserBrowsingHistorySerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        # 获取 user_id
        user_id = request.user.id
        # 先查询 redis，获取 list 类型数据
        redis_conn = get_redis_connection("history")
        # lrange(objects, index_start, index_end)
        sku_id_list = redis_conn.lrange(f"history_{user_id}", 0, constants.USER_BROWSE_HISTORY_MAX_LIMIT - 1)
        # 查询 数据库
        # 这样查询的结果是根据mysql数据库中的id排序确定的，做法不严谨
        # sku_object_list = SKU.objects.filter(id__in=sku_id_list)
        sku_object_list = list()
        try:
            for sku_id in sku_id_list:
                sku = SKU.objects.get(id=sku_id)
                sku_object_list.append(sku)
        except Exception as e:
            logger.error(f"数据库查询异常:[message: {e}]")
            return Response({"message": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # 序列化返回
        serializer = serializers.SKUSerializer(sku_object_list, many=True)
        return Response(serializer.data)


class AddressViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    """ 新增/修改 用户收货地址 """

    serializer_class = serializers.UserAddressSerializer
    permission_classes = [IsAuthenticated]     # 指明必须登录认证后才能访问

    def get_queryset(self):
        return self.request.user.addresses.filter(is_deleted=False)

    # GET /addresses/
    def list(self, request, *args, **kwargs):
        """ 用户地址列表数据 """
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        user = self.request.user
        data = {
            "user_id": user.id,
            "default_address_id": user.default_address_id,
            "limit": constants.USER_ADDRESS_COUNTS_LIMIT,
            "addresses": serializer.data,
        }

        return Response(data)

    # POST /addresses/
    def create(self, request, *args, **kwargs):
        """ 保存用户收货地址 """
        # 检查用户地址数量是否超过上限
        count = request.user.addresses.count()
        if count >= constants.USER_ADDRESS_COUNTS_LIMIT:
            return Response({"message": "收货地址数量已达到上限"}, status=status.HTTP_400_BAD_REQUEST)
        return super().create(request, *args, **kwargs)

    # DELETE /addresses/<pk>/
    def destroy(self, request, *args, **kwargs):
        """ 删除收货地址 """
        address = self.get_object()
        # 进行逻辑删除
        address.is_deleted = True
        address.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    # PUT /addresses/pk/status/
    @action(methods=["put"], detail=True)
    def status(self, request, pk=None):
        """ 设置默认地址 """
        address = self.get_object()
        request.user.default_address = address
        request.user.save()
        return Response({"message": "OK"}, status=status.HTTP_200_OK)

    # PUT /addresses/pk/title/
    # 需要请求体参数 title
    @action(methods=["put"], detail=True)
    def title(self, request, pk=None):
        """ 修改标题 """
        address = self.get_object()
        serializer = serializers.AddressTitleSerializer(instance=address, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


# GET /email/verification/?token=xxx
class VerifyEmailView(APIView):
    """ 邮箱验证 """
    def get(self, request):
        # 获取token
        token = request.query_params.get('token')
        if not token:
            return Response({"message": "缺少token参数"}, status=status.HTTP_400_BAD_REQUEST)

        # 验证token
        user = User.check_verify_email_token(token)
        if not user:
            return Response({"message": "链接信息失效"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            user.email_active = True
            user.save()
            return Response({"message": "邮箱激活完成"})


# PUT /email/
class EmailView(UpdateAPIView):
    """ 绑定邮箱，发送验证邮件"""
    serializer_class = serializers.EmailSerializer
    permission_classes = [IsAuthenticated]  # 指明必须登录认证后才能访问

    def get_object(self):
        return self.request.user

    # def put(self):
        # 获取email
        # 校验email
        # 查询 user
        # 更新数据
        # 序列化返回


# GET /user/
class UserDetailView(RetrieveAPIView):
    """获取用户基本信息"""
    serializer_class = serializers.UserDetailSerializer
    permission_classes = [IsAuthenticated]  # 指明必须登录认证后才能访问

    def get_object(self):
        # 返回当前请求的用户
        # 在类视图对象中，可以通过类视图对象的属性获取 request
        # 在 django 的请求 request 对象中，user属性表明当前请求的用户
        return self.request.user


# url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
class MobileCountView(APIView):
    """
    校验手机号是否存在
    """
    def get(self, request, mobile):
        # 操作数据库，查询用户名是否存在
        try:
            count = User.objects.filter(mobile=mobile).count()
        except Exception as e:
            logger.error(f"数据库查询异常:[message: {e}]")
            return Response({"message": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        data = {
            "mobile": mobile,
            "count": count
        }

        return Response(data)


# url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
class UsernameCountView(APIView):
    """
    校验用户名是否存在
    """
    def get(self, request, username):
        # 操作数据库，查询用户名是否存在
        try:
            count = User.objects.filter(username=username).count()
        except Exception as e:
            logger.error(f"数据库查询异常:[message: {e}]")
            return Response({"message": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        data = {
            "username": username,
            "count": count
        }

        return Response(data)


# 添加合并购物车装饰器
# url(r'^users/$', views.UserView.as_view())
@method_decorator(merge_cart_decoration, name="post")
class UserView(CreateAPIView):
    """
    用户注册
    传入参数：username, password, password2, sms_code, mobile, allow
    返回参数：
    """
    serializer_class = CreateUserSerializer