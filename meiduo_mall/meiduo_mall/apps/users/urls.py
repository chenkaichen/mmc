from django.conf.urls import url
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token
from . import views


urlpatterns =[
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
    url(r'^users/$', views.UserView.as_view()),     # 提交注册
    # url(r'^authorizations/$', obtain_jwt_token),     # 登录认证
    url(r'^authorizations/$', views.MyObtainJSONWebToken.as_view()),     # 登录认证，补充了合并购物车
    url(r'^user/$', views.UserDetailView.as_view()),     # 用户个人中心
    url(r'^email/$', views.EmailView.as_view()),     # 保存邮箱，发送激活链接邮件
    url(r'^email/verification/$', views.VerifyEmailView.as_view()),     # 邮箱激活
    url(r'^browse_histories/$', views.UserBrowsingHistoryView.as_view()),     # 用户浏览记录

]

router = routers.DefaultRouter()
router.register(r'addresses', views.AddressViewSet, base_name='addresses')
urlpatterns += router.urls

# POST /addresses/ 新建  -> create
# PUT /addresses/<pk>/ 修改  -> update
# GET /addresses/  查询  -> list
# DELETE /addresses/<pk>/  删除 -> destroy
# PUT /addresses/<pk>/status/ 设置默认 -> status
# PUT /addresses/<pk>/title/  设置标题 -> title