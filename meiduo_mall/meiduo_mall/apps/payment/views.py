from alipay import AliPay
import os
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from orders.models import OrderInfo
from rest_framework.permissions import IsAuthenticated


# PUT /payment/status/?支付宝回调(return_url)时携带的参数
# return_url="http://www.meiduo.site:8080/pay_success.html"
from payment.models import Payment


class PaymentStatusView(APIView):
    """ 支付结果 """
    def put(self, request):
        # 获取查询字符串数据
        # out_trade_no: 订单号，    trade_no: 交易流水号
        # total_amount: 订单总金额   seller_id: 支付宝唯一用户号
        alipay_request_dict = request.query_params  # QueryDict

        # 如果查询字符串为空，前端没有将支付宝回调时携带的参数传递过来
        if not alipay_request_dict:
            return Response({"message": "缺少参数"}, status=status.HTTP_400_BAD_REQUEST)

        # 转换成python中的字典
        data = alipay_request_dict.dict()
        # 用pop方法取出签名，接口文档中推荐使用的方法
        sign = data.pop("sign")

        # 校验参数，使用AliPay模块来验证前端传过来的数据是否真的是支付宝在回调时携带的参数
        alipay_client = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,  # 默认回调url
            app_private_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "keys/app_private_key.pem"),
            alipay_public_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                "keys/alipay_public_key.pem"),  # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥,
            sign_type="RSA2",  # RSA 或者 RSA2
            debug=settings.ALIPAY_DEBUG  # 默认False，是否是沙箱环境
        )

        # verify函数返回验证结果，True 或 False
        result = alipay_client.verify(data, sign)
        if result:
            # 保存支付结果数据
            # out_trade_no: 订单号，    trade_no: 交易流水号
            # total_amount: 订单总金额   seller_id: 支付宝唯一用户号

            order_id = data.get("out_trade_no")     # 订单编号
            trade_id = data.get("trade_no")         # 交易流水号
            Payment.objects.create(
                order_id=order_id,
                trade_id=trade_id,
            )

            # 修改订单状态
            OrderInfo.objects.filter(order_id=order_id).update(status=OrderInfo.ORDER_STATUS_ENUM["UNSEND"])

            # 返回 trade_id（交易流水号）
            return Response({"trade_id": trade_id})
        else:
            # 返回参数错误
            return Response({"message": "参数错误"}, status=status.HTTP_400_BAD_REQUEST)


# GET /orders/(?P<order_id>\d+)/payment/
class PaymentView(APIView):
    """ 支付 """

    permission_classes = [IsAuthenticated]

    def get(self, request, order_id):
        """ 获取支付链接 """

        # 获取参数：order_id, user
        user = request.user
        # 校验订单信息
        try:
            order = OrderInfo.objects.get(
                order_id=order_id,
                user=user,
                status=OrderInfo.ORDER_STATUS_ENUM["UNPAID"],
                pay_method=OrderInfo.PAY_METHODS_ENUM["ALIPAY"],
            )
        except OrderInfo.DoesNotExist:
            return Response({"message": "订单信息有误"},  status=status.HTTP_400_BAD_REQUEST)

        # 向支付宝发起请求，获取支付链接参数
        alipay_client = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,  # 默认回调url，notify_url 需要公网的环境
            app_private_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "keys/app_private_key.pem"),
            alipay_public_key_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "keys/alipay_public_key.pem"),  # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥,
            sign_type="RSA2",  # RSA 或者 RSA2
            debug=settings.ALIPAY_DEBUG  # 默认False
        )

        # 电脑网站支付，需要跳转到 https://openapi.alipay.com/gateway.do? + order_string
        order_string = alipay_client.api_alipay_trade_page_pay(
            out_trade_no=order_id,  # 订单编号
            total_amount=str(order.total_amount),   # 订单总金额，在数据库中是 Decimal 类型，需要转换
            subject=f"美多商城{order_id}",    # 订单标题，可以自己指定
            return_url="http://www.meiduo.site:8080/pay_success.html",  # 支付成功回调url
            notify_url=None     # 可选，不填则使用默认notify_url
        )

        # 拼接支付链接地址
        alipay_url = settings.ALIPAY_URL + "?" + order_string
        return Response({'alipay_url': alipay_url})
