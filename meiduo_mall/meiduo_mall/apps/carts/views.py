from django.core.serializers import get_serializer
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
import logging
from carts.utils import CartMixin
from goods.models import SKU
from .serializers import CartSerializer, CartSKUSerializer, CartDeleteSerializer, CartSelectAllSerializer

logger = logging.getLogger("django")


class CartSelectAllView(CartMixin, GenericAPIView):
    """ 购物车全选 """

    serializer_class = CartSelectAllSerializer

    def put(self, request):
        # selected
        # 校验
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # 获取校验通过的数据
        selected = serializer.validated_data["selected"]

        # 读取购物车数据
        cart_dict = self.read_cart(request)

        # 修改购物车数据
        for sku_key in cart_dict:
            cart_dict[sku_key][1] = selected

        # 构建响应数据
        response = Response(serializer.data)
        # 保存购物车数据
        self.wright_cart(request, cart_dict, response)

        return response


class CartView(CartMixin, GenericAPIView):
    """ 购物车 """
    serializer_class = CartSerializer

    def post(self, request):
        """ 将商品保存到购物车 """
        # sku_id, count, selected
        # 使用序列化器进行校验
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # 获取校验通过的数据
        # sku_id, count, selected
        attrs = serializer.validated_data
        sku_id, count, selected = attrs['sku_id'], attrs['count'], attrs['selected']

        # 读取购物车数据
        cart_dict = self.read_cart(request)

        # 根据幂等性进行处理
        cart_dict[sku_id] = [count, selected]

        # 保存购物车数据
        response = Response(serializer.data)
        self.wright_cart(request, cart_dict, response)

        return response

    def get(self, request):
        """ 查询购物车 """

        # 查询
        cart_dict = self.read_cart(request)

        sku_id_list = cart_dict.keys()
        skus = list()
        # 查询数据库，获取商品sku对象
        try:
            skus = SKU.objects.filter(id__in=sku_id_list)
        except Exception as e:
            logger.error(f"数据库查询异常:[message: {e}]")
        # 遍历sku_obj_list 向sku对象中添加 count 和 selected 属性
        for sku in skus:
            sku.count = cart_dict[sku.id][0]
            sku.selected = cart_dict[sku.id][1]

        # 序列化返回
        serializer = CartSKUSerializer(skus, many=True)
        return Response(serializer.data)

    def put(self, request):
        """ 修改购物车数据 """
        # sku_id, count, selected
        # 使用序列化器进行校验
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # 获取校验通过的数据
        attrs = serializer.validated_data
        sku_id, count, selected = attrs['sku_id'], attrs['count'], attrs['selected']

        # 读取购物车数据
        cart_dict = self.read_cart(request)

        # 修改购物车数据
        cart_dict[sku_id] = [count, selected]

        # 保存购物车数据
        response = Response(serializer.data)
        self.wright_cart(request, cart_dict, response)

        return response

    def delete(self, request):
        """ 删除购物车中的商品 """
        # sku_id
        # 校验参数
        serializer = CartDeleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sku_id = serializer.validated_data["sku_id"]

        # 读取购物车数据
        cart_dict = self.read_cart(request)

        # 删除购物车数据
        try:
            cart_dict.pop(sku_id)
        except Exception:
            return Response({"message": "购物车中没有这件商品"}, status=status.HTTP_404_NOT_FOUND)

        # 保存购物车数据
        response = Response(serializer.data)
        self.wright_cart(request, cart_dict, response)

        return response
