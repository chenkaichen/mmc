import base64
import pickle
from django_redis import get_redis_connection
from carts import constants

# redis 存储方式为字符串类型：
# set key value (user_id，{sku_id: [count, selected]})
class CartMixin(object):
    """ 自定义辅助类 """

    def read_cart(self, request) -> dict:
        """ 读取购物车数据 """

        # 获取用户登录状态
        user = request.user
        # 如果用户存在且通过认证
        if user and user.is_authenticated:
            # 读取 redis 中购物车数据，返回字典数据
            return self.read_from_redis(user)
        else:
            # 读取 cookie 中购物车数据，返回字典数据
            return self.read_from_cookie(request)

    def wright_cart(self, request, cart_dict, response):
        """ 写入购物车数据 """

        # 获取用户登录状态
        user = request.user
        # 如果用户存在且通过验证
        if user and user.is_authenticated:
            # 将商品保存到 redis
            return self.wright_to_redis(request, cart_dict)
        else:
            # 将商品保存到 cookie
            return self.wright_to_cookie(cart_dict, response)

    def read_from_redis(self, user) -> dict:
        """ 读取 redis 中的购物车数据，返回字典 """
        redis_conn = get_redis_connection("cart")
        # 从 redis 取出来的值是 <class 'bytes'> 类型
        cart_bytes = redis_conn.get(f"cart_{user.id}")
        cart_dict = pickle.loads(base64.b64decode(cart_bytes)) if cart_bytes else dict()
        return cart_dict

    def wright_to_redis(self, request, cart_dict):
        """ 商品保存到redis """
        redis_conn = get_redis_connection("cart")
        cart_str = base64.b64encode(pickle.dumps(cart_dict)).decode()
        redis_conn.set(f"cart_{request.user.id}", cart_str)

    def read_from_cookie(self, request) -> dict:
        """ 读取 cookie 中的购物车数据，返回字典 """
        cart_str = request.COOKIES.get("cart")      # <class 'str'>
        cart_dict = pickle.loads(base64.b64decode(cart_str.encode())) if cart_str else dict()
        return cart_dict

    def wright_to_cookie(self, cart_dict, response):
        """ 将商品信息保存到 cookie 中 """
        cart_str = base64.b64encode(pickle.dumps(cart_dict)).decode()
        response.set_cookie('cart', cart_str, max_age=constants.CART_COOKIE_EXPIRES)


def merge_cart_cookie_to_redis(request, user, response):
    """ 登录时合并购物车，将cookie中的数据合并到redis中 """
    # 相同商品如何处理？
    # 商品数量：以cookie为准
    # 勾选状态：以cookie为准

    # 1 业务处理，实例化对象
    merge_cart = CartMixin()
    # 2 获取redis中的购物车数据
    redis_cart_dict = merge_cart.read_from_redis(user)
    # 3 获取cookie中的购物车数据
    cookie_cart_dict = merge_cart.read_from_cookie(request)
    # 4 合并
    # new_cart_dict = redis_cart_dict.update(cookie_cart_dict)
    redis_cart_dict.update(cookie_cart_dict)
    # 5 将购物车数据保存到redis中
    merge_cart.wright_to_redis(request, redis_cart_dict)
    # 6 删除cookie中的购物车数据
    response.delete_cookie("cart")


""" 使用装饰器的方式实现合并购物车 """
def merge_cart_decoration(func):
    def wrapper(request, *args, **kwargs):
        # 执行视图

        print("调用原始方法" + request.method)

        resp = func(request, *args, **kwargs)

        if 200 <= resp.status_code < 300 :
            # 判断视图执行结果，如果视图成功执行，且用户登录认证成功，则进行合并购物车操作
            if request.user and request.user.is_authenticated:
                merge_cart_cookie_to_redis(request, request.user, resp)
                print("合并购物车成功")
            else:
                # raise Exception("如果登录成功，请添加 user 属性到 request 中")
                print("如果登录成功，请添加 user 属性到 request 中")
        else:
            print("身份认证错误，没有合并购物车")
        return resp

    return wrapper