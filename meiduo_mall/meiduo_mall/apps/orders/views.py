from decimal import Decimal

from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from carts.utils import CartMixin
from goods.models import SKU
from orders.serializers import SaveOrderSerializer, OrderSettlementSerializer, CartSKUSerializer


class SaveOrderView(CreateAPIView):
    """ 保存订单 """

    serializer_class = SaveOrderSerializer
    permission_classes = [IsAuthenticated]


""" 在序列化器中实现 """
    # def post(self):
        # 接收参数
        # 校验数据
        # 获取购物车勾选结算的数据
        # 创建订单保存
        # 序列化返回


class OrderSettlementView(CartMixin, APIView):
    """ 订单结算页面 """

    permission_classes = [IsAuthenticated]

    def get(self, request):

        # 从购物车中获取用户勾选的商品数据(redis)
        cart_dict = self.read_cart(request)
        # 筛选已勾选的商品的sku_id
        sku_id_list = [key for key in cart_dict.keys() if cart_dict[key][1]]

        # 如果没有勾选商品
        if not sku_id_list:
            return Response({"message": "请选择商品"}, status=status.HTTP_404_NOT_FOUND)

        # 查询数据库，获取已勾选商品对象
        skus = SKU.objects.filter(id__in=sku_id_list)

        # 在商品sku对象中添加count属性
        for sku in skus:
            sku.count = cart_dict[sku.id][0]

        # 添加运费数据
        freight = Decimal('8.00')

        # 序列化返回，第一种方式
        # 使用序列化器返回数据(这里使用了嵌套的序列化器)
        # serializer = CartSKUSerializer(skus, many=True)
        # return Response({"freight": freight, "skus": serializer.data})

        # 第二种方式：序列化的嵌套
        serializer = OrderSettlementSerializer({"freight": freight, "skus": skus})
        return Response(serializer.data)