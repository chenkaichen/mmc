from django.db import transaction
from django.utils import timezone
from rest_framework import serializers
from goods.models import SKU
from orders.models import OrderInfo, OrderGoods
from carts.utils import CartMixin
from decimal import Decimal
from django.db.models import F
import logging


logger = logging.getLogger("django")


class SaveOrderSerializer(CartMixin, serializers.ModelSerializer):
    """ 保存订单-序列化器 """

    class Meta:
        model = OrderInfo
        fields = ("address", "pay_method", "order_id")
        read_only_fields = ("order_id", )
        extra_kwargs = {
            'address': {
                'write_only': True,
                'required': True,
            },
            'pay_method': {
                'write_only': True,
                'required': True
            }
        }

    def create(self, validated_data):
        """保存订单"""

        # 获取当前请求的request对象
        request = self.context["request"]
        # 获取当前用户
        user = request.user

        # 从redis中获取购物车结算商品数据
        cart_dict = self.read_cart(request)
        # 获取结算商品的id列表
        sku_id_list = [key for key in cart_dict.keys() if cart_dict[key][1]]

        # 如果没有结算的商品，抛出异常
        if not sku_id_list:
            raise serializers.ValidationError("没有需要结算的商品")

        # 保存订单基本信息数据 OrderInfo
        """ 创建事务，开启一个事务 """
        with transaction.atomic():
            """ 创建一个保存点 """
            save_id = transaction.savepoint()

            try:
                # 1.组织订单编号 20180903153611+ {user.id}
                # timezone.now() -> datetime
                order_id = timezone.now().strftime('%Y%m%d%H%M%S') + ('%09d' % user.id)

                # 2.获取验证通过的数据
                address, pay_method = validated_data["address"], validated_data["pay_method"]

                # 3.生成订单记录
                order = OrderInfo.objects.create(
                    order_id=order_id,
                    user=user,
                    address=address,
                    total_count=0,      # 商品总数
                    total_amount=Decimal(0),     # 商品总金额
                    freight=Decimal("8.00"),
                    pay_method=pay_method,
                    status=OrderInfo.ORDER_STATUS_ENUM["UNSEND"] if pay_method == OrderInfo.PAY_METHODS_ENUM["CASH"] else OrderInfo.ORDER_STATUS_ENUM["UNPAID"]
                )

                # 遍历结算商品：
                # 后面使用 F 对象来查询实时的库存信息
                # skus = SKU.objects.filter(id__in=sku_id_list)
                for sku_id in sku_id_list:

                    while True:
                        # 查询数据库获取商品sku对象
                        sku = SKU.objects.get(id=sku_id)
                        # 获取购买商品的数量
                        sku_count = cart_dict[sku.id][0]
                        # 获取当前的库存 和 销量
                        origin_stock = sku.stock
                        origin_sales = sku.sales
                        # 判断商品库存是否充足
                        if origin_stock < sku_count:
                            raise serializers.ValidationError("商品库存不足")

                        # 用于演示并发下单
                        # import time
                        # time.sleep(5)

                        # new_stock = origin_stock - sku_count  # 减少商品库存
                        # new_sales = origin_sales + sku_count  # 增加商品销量
                        # sku.save()  # 保存到数据库
                        """ 使用乐观锁进行处理，一步完成数据库的查询和更新 """
                        # update 返回受影响的行数，使用 F 对象来查询实时的库存信息
                        result = SKU.objects.filter(id=sku.id, stock=origin_stock).update(stock=F("stock") - sku_count, sales=F("sales") + sku_count)
                        if result == 0:
                            # 表示更新失败，有人抢先一步买了商品，再一次判断库存
                            continue

                        # 累计到商品的 SPU 销量信息
                        sku.goods.sales += sku_count
                        sku.goods.save()

                        # 累计订单记录中的商品总数和商品总金额
                        order.total_count += sku_count
                        order.total_amount += (sku.price * sku_count)

                        # 保存订单商品数据
                        OrderGoods.objects.create(
                            order=order,
                            sku=sku,
                            count=sku_count,
                            price=sku.price
                        )
                        break

                    # 在商品总金额的基础上增加运费
                    order.total_amount += order.freight
                    order.save()

            # except serializers.ValidationError:
            #     # 回滚到保存点(save_id)
            #     transaction.savepoint_rollback(save_id)
            #     raise
            except Exception as e:
                logger.error(f"保存订单异常:[message: {e}]")
                # 回滚到保存点(save_id)
                transaction.savepoint_rollback(save_id)
                raise
            else:
                """ 提交事务 """
                transaction.savepoint_commit(save_id)

                # 在redis购物车中删除已结算商品的数据
                cart_dict = {key: value for key, value in cart_dict.items() if not cart_dict[key][1]}
                # 将数据存储到redis
                self.wright_to_redis(request, cart_dict)

                # 返回
                return order


class CartSKUSerializer(serializers.ModelSerializer):
    """ 购物车商品数据序列化器 """
    count = serializers.IntegerField(label='数量')

    class Meta:
        model = SKU
        fields = ('id', 'name', 'default_image_url', 'price', 'count')


class OrderSettlementSerializer(serializers.Serializer):
    """ 订单结算数据序列化器 """
    freight = serializers.DecimalField(label='运费', max_digits=10, decimal_places=2)
    skus = CartSKUSerializer(many=True)