import json
from urllib.request import urlopen
from oauth.exceptions import OAuthQQAPIError
from . import constants
import urllib.parse
import logging
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadData

logger = logging.getLogger('django')


class OAuthQQ(object):
    """
    QQ认证 辅助工具类
    """

    # 给每个对象初始化对应的参数（不能定义为类属性）
    def __init__(self, client_id=None, redirect_uri=None, state=None, client_secret=None):
        self.client_id = client_id if client_id else constants.QQ_CLIENT_ID
        self.redirect_uri = redirect_uri if redirect_uri else constants.QQ_REDIRECT_URI
        self.state = state if state else constants.QQ_STATE
        self.client_secret = client_secret if client_secret else constants.QQ_CLIENT_SECRET

    def get_login_url(self):
        """拼接QQ登录地址"""
        url = 'https://graph.qq.com/oauth2.0/authorize?'
        params = {
            'response_type': 'code',
            'client_id': self.client_id,
            'redirect_uri': self.redirect_uri,
            'state': self.state
        }

        url += urllib.parse.urlencode(params)
        return url

    def get_access_token(self, code):
        """获取 access_token"""
        url = 'https://graph.qq.com/oauth2.0/token?'

        params = {
            'grant_type': 'authorization_code',
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'code': code,
            'redirect_uri': self.redirect_uri
        }

        # 组织请求url地址
        url += urllib.parse.urlencode(params)
        try:
            # 发送请求
            resp = urlopen(url)

            # 获取响应体数据
            resp_data = resp.read().decode()    # 获取的是字节数据，需要解码

            # access_token = FE04 ** ** ** ** ** ** ** ** ** ** ** ** CCE2 & expires_in = 7776000 & refresh_token = 88E4 ** ** ** ** ** ** ** ** ** ** ** ** BE14

            # 解析成字典
            resp_dict = urllib.parse.parse_qs(resp_data)
            access_token = resp_dict.get("access_token")[0]     # access_token是列表类型
        except Exception as e:
            logger.error(f"获取access_token异常:[message: {e}]")
            raise OAuthQQAPIError

        return access_token

    def get_openid(self, access_token):
        """获取 openid"""
        # 组织请求url地址
        url = 'https://graph.qq.com/oauth2.0/me?access_token=' + access_token
        try:
            # 发送请求
            resp = urlopen(url)

            # 获取响应体数据
            resp_data = resp.read().decode()  # 获取的是字节数据，需要解码

            # callback({"client_id": "YOUR_APPID", "openid": "YOUR_OPENID"});

            # 切片解析
            resp_data = resp_data[10:-4]
            resp_dict = json.loads(resp_data)
            openid = resp_dict.get("openid")
        except Exception as e:
            logger.error(f"获取openid异常:[message: {e}]")
            raise OAuthQQAPIError

        return openid

    @staticmethod
    def generate_save_user_token(openid):
        """
        生成保存用户数据的token
        :param openid: 用户的openid
        :return: token
        """
        serializer = Serializer(constants.SECRET_KEY, expires_in=constants.BIND_USER_TOKEN_EXPIRES)
        data = {'openid': openid}
        access_token = serializer.dumps(data).decode()
        return access_token

    @staticmethod
    def check_save_user_token(token):
        """
        检验保存用户数据的token
        :param token: token
        :return: openid or None
        """
        serializer = Serializer(constants.SECRET_KEY, expires_in=constants.BIND_USER_TOKEN_EXPIRES)
        try:
            data = serializer.loads(token)
        except BadData:
            return
        else:
            return data.get('openid')