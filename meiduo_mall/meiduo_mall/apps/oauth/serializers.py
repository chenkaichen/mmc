from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
import logging

from oauth.models import OAuthQQUser
from oauth.utils import OAuthQQ
from users.models import User


logger = logging.getLogger('django')


class OAuthQQUserSerializer(serializers.ModelSerializer):
    """定义绑定QQ用户的序列化器类"""
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    access_token = serializers.CharField(label='操作凭证', write_only=True)
    token = serializers.CharField(read_only=True)
    # mobile需要自己声明：因为用户传入的手机号已存在会验证失败，在流程中需要对已存在的手机号进行绑定
    mobile = serializers.RegexField(label='手机号', regex=r'^1[3-9]\d{9}$')

    class Meta:
        model = User
        fields = ('mobile', 'password', 'sms_code', 'access_token', 'id', 'username', 'token')

        extra_kwargs = {
            'username': {
                'read_only': True
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    def validate(self, attrs):
        # 检验access_token 获取 openid
        access_token = attrs['access_token']
        openid = OAuthQQ.check_save_user_token(access_token)
        if not openid:
            raise serializers.ValidationError('无效的access_token')

        attrs['openid'] = openid

        # 检验短信验证码
        mobile = attrs['mobile']
        sms_code = attrs['sms_code']
        try:
            redis_conn = get_redis_connection('verify_codes')
            real_sms_code = redis_conn.get('sms_%s' % mobile)
        except Exception as e:
            logger.error(f"数据库查询错误:[massage: {e}]")
            raise serializers.ValidationError('数据库查询错误')
        if real_sms_code.decode() != sms_code:
            raise serializers.ValidationError('短信验证码错误')

        # 如果用户存在，校验用户密码
        try:
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            # 如果用户不存在，在下面创建用户
            pass
        else:
            password = attrs['password']
            if not user.check_password(password):
                raise serializers.ValidationError('密码错误')
            attrs['user'] = user
        return attrs

    def create(self, validated_data):
        openid = validated_data['openid']
        user = validated_data.get('user')   # user 可能不存在
        mobile = validated_data['mobile']
        password = validated_data['password']

        try:
            if not user:
                # 如果用户不存在，创建用户，绑定openid（创建OAuthQQUser对象）
                # create_user方法能够将密码进行加密处理
                user = User.objects.create_user(username=mobile, mobile=mobile, password=password)

            OAuthQQUser.objects.create(user=user, openid=openid)
        except Exception as e:
            logger.error(f"绑定用户信息失败:[massage: {e}]")
            raise serializers.ValidationError('绑定用户信息失败')

        # 签发JWT token
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        user.token = token

        # 给首次通过QQ登录的用户添加user属性，方便后面合并购物车
        self.context["request"].user = user

        return user