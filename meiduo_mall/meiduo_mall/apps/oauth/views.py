from django.shortcuts import render
from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from carts.utils import merge_cart_decoration
from oauth.exceptions import OAuthQQAPIError
from oauth.models import OAuthQQUser
from oauth.serializers import OAuthQQUserSerializer
from oauth.utils import OAuthQQ


#  url(r'^qq/authorization/$', views.QQAuthURLView.as_view()),
class QQAuthURLView(APIView):
    """
    QQ登录的url  ？next=xxx
    """
    def get(self, request):
        """
        提供用于qq登录的url接口
        """
        # 获取请求参数中的url
        next = request.query_params.get('next')     # request.query_params是query_dict对象

        # 拼接QQ登录的网址
        oauth = OAuthQQ(state=next)
        login_url = oauth.get_login_url()
        data = {
            "login_url": login_url
        }
        return Response(data)


# GET /oauth/qq/user/?code=xxx
# url(r'^qq/user/$', views.QQAuthUserView.as_view()),
@method_decorator(merge_cart_decoration, name="get")    # 通过QQ登录的用户，合并购物车
@method_decorator(merge_cart_decoration, name="post")   # 首次通过QQ登录的用户，合并购物车
class QQAuthUserView(CreateAPIView):
    """
    QQ登录的用户 ？code=xxx
    """
    # 用户首次使用QQ登录，绑定用户信息，使用序列化器完成
    serializer_class = OAuthQQUserSerializer

    def get(self, request):
        # 获取code
        code = request.query_params.get("code")
        if not code:
            return Response({"message": "缺少参数code"}, status=status.HTTP_400_BAD_REQUEST)

        oauth_qq = OAuthQQ()
        try:
            # 凭借code 获取 access_token
            access_token = oauth_qq.get_access_token(code)
            # 凭借 access_token 获取 openid
            openid = oauth_qq.get_openid(access_token)
        except OAuthQQAPIError:
            return Response({"message": "访问QQ接口异常"}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        # 根据 openid 查询数据库 OAuthQQUser，判断用户是否存在
        oauth_qq_user = None
        try:
            oauth_qq_user = OAuthQQUser.objects.get(openid=openid)
        except OAuthQQUser.DoesNotExist:
            # 用户第一次使用QQ登录，处理 openid 并返回 access_token
            access_token = oauth_qq.generate_save_user_token(openid)
            data = {
                "access_token": access_token
            }
            return Response(data)

        else:
            # 如果数据存在，表示用户已经绑定过身份，签发 JWT_token
            # 获取user对象
            user = oauth_qq_user.user

            jwt_payload_hendler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_hendler = api_settings.JWT_ENCODE_HANDLER

            payload = jwt_payload_hendler(user)
            token = jwt_encode_hendler(payload)

            # 给通过QQ登录的用户请求中添加user属性，方便后面合并购物车
            request.user = user

            data = {
                "username": user.username,
                "user_id": user.id,
                "token": token
            }
            return Response(data)


    # def post(self, request):
    """
        用户首次使用QQ登录，绑定用户信息
        :param request: mobile, password, sms_code, access_token
        :return:
    """
        # 获取数据

        # 校验数据

        # 判断用户是否存在

        # 如果存在，进行绑定，创建OAuthQQUser对象

        # 如果不存在，先创建User，再创建OAuthQQUser对象

        # 签发JWT token
