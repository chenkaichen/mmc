from rest_framework.routers import DefaultRouter
from areas import views

urlpatterns = list()

router = DefaultRouter()
router.register(r'areas', views.AreasViewSet, base_name='areas')
urlpatterns += router.urls

# /areas/   {"get": "list"} 只返回顶级数据 parent=None, name=xxx
# /areas/<pk>   {"get": "retrieve"}  name=xxx
