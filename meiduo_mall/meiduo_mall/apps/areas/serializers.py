from rest_framework import serializers

from .models import Area


class AreaSerializer(serializers.ModelSerializer):
    """
    行政区划信息 序列化器
    """
    class Meta:
        model = Area
        fields = ('id', 'name')
# wh_area = Area.objects.get(id=100000)
# wh_area.id
# wh_area.name
# wh_area.subs = [area_object, area_object, ...]


class SubAreaSerializer(serializers.ModelSerializer):
    """
    子行政区划信息序列化器
    """
    subs = AreaSerializer(many=True, read_only=True)

    class Meta:
        model = Area
        fields = ('id', 'name', 'subs')