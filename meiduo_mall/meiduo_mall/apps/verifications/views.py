import random
from django.http import HttpResponse
from django_redis import get_redis_connection
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from meiduo_mall.libs.captcha.captcha import captcha
import logging

from verifications import constants
from verifications.serializers import ImageCodeCheckSerializer
from celery_tasks.sms.utils.yuntongxun import sms


logger = logging.getLogger("django")


class ImageCodeView(APIView):
    """图片验证码"""
    def get(self, request, image_code_id):

        # 生成验证码图片
        text, image = captcha.generate_captcha()

        # 保存验证码内容(为的是后面做校验)
        redis_conn = get_redis_connection('verify_codes')
        redis_conn.setex("img_%s" % image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES, text)
        logger.info(f"图片验证码内容:[image_code: {text}]")

        # 返回验证码图片
        return HttpResponse(image, content_type='image/jpg')


#  GET /sms_codes/(?P<mobile>1[3-9]\d{9})/?image_code_id=xxx&text=xxx
class SMSCodeView(GenericAPIView):
    """
    短信验证码
    传入参数：mobile, image_code_id, text
    """
    # 指明序列化器类
    serializer_class = ImageCodeCheckSerializer

    def get(self, request, mobile):
        # 校验参数（由序列化器完成）

        # 准备数据传入序列化器（方法一）
        data = dict()
        # 因为query_params实例是不可变的，所以将query_params取出来放到新的字典中
        # 直接update结果：{'text': ['WIGG'], 'image_code_id': ['32a55c6a-218b-46b1-ba69-bc4582d7a7dd'], 'mobile': '13018083059'}
        data.update(request.query_params.dict())    # 这里的dict方法是将查询字典中的数据遍历出来
        # data = {key: value for key, value in request.query_params.items()}
        data["mobile"] = mobile

        # 准备数据传入序列化器（方法二）
        # data=request.query_params

        # 将数据传入序列化器校验
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)

        # 生成6位数短信验证码（random）
        sms_code = "%06d" % random.randint(0, 999999)

        # 保存短信验证码，保存发送记录
        # redis_conn = get_redis_connection('verify_codes')
        # redis_conn.setex(f"sms_{mobile},", constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        # redis_conn.setex(f"send_flag_{mobile}", constants.SEND_SMS_INTERVAL, "T")

        try:
            # 使用redis管道保存短信验证码、发送记录，并添加异常捕获
            redis_conn = get_redis_connection('verify_codes')
            pl = redis_conn.pipeline()
            pl.setex(f"sms_{mobile}", constants.SMS_CODE_REDIS_EXPIRES, sms_code)
            pl.setex(f"send_flag_{mobile}", constants.SEND_SMS_INTERVAL, "T")
            # 让管道通知redis执行命令
            pl.execute()
        except Exception as e:
            # redis存储异常
            logger.error(f"redis存储异常:[message: {e}]")
            return Response({"message": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # 根据短信模板整理发送内容
        # try:
        #     expires = str(constants.SMS_CODE_REDIS_EXPIRES // 60)
        #     ccp = sms.CCP()
        #     result = ccp.send_template_sms(mobile, [sms_code, expires], constants.SMS_TEMPLATE_ID)
        # except Exception as e:
        #     # 发送短信异常
        #     logger.error(f"发送短信验证码异常:[mobile: {mobile}, message: {e}]")
        #     return Response({"message": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        #
        # # 如果发送短信没有异常，则获取发送短信的状态
        # else:
        #     if result == 0:
        #         logger.info(f"发送短信验证码成功:[mobile: {mobile}]")
        #         return Response({"message": "OK"})
        #     else:
        #         logger.info(f"发送短信验证码失败:[mobile: {mobile}]")
        #         return Response({"message": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        """发送短信验证码: 使用celery实现"""
        from celery_tasks.sms.tasks import send_sms_code
        send_sms_code.delay(mobile, sms_code)
        logger.info(f"发送短信验证码成功:[sms_code: {sms_code}]")
        return HttpResponse({"massage": "OK"})
