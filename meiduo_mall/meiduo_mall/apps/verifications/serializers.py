from django_redis import get_redis_connection
from rest_framework import serializers


class ImageCodeCheckSerializer(serializers.Serializer):
    """
    图片验证码的校验序列化器
    """
    image_code_id = serializers.UUIDField()
    text = serializers.CharField(max_length=4, min_length=4)
    mobile = serializers.CharField(max_length=11, min_length=11)

    def validate(self, attrs):
        """校验"""
        image_code_id = attrs['image_code_id']
        text = attrs['text']
        mobile = attrs['mobile']

        # 查询真实图片验证码
        try:
            redis_conn = get_redis_connection('verify_codes')
            real_image_code_text = redis_conn.get('img_%s' % image_code_id)
            if not real_image_code_text:
                raise serializers.ValidationError('图片验证码无效')
        except Exception as e:
            raise serializers.ValidationError(e)

        # 讲道理，用户只有一次验证机会，这样才显得严谨
        # 在查询验证码之后删除redis中的图片验证码
        try:
            redis_conn.delete(f"img_{image_code_id}")
        except Exception as e:
            raise serializers.ValidationError(e)

        # 比较图片验证码，从redis中取出的图片验证码是字节数据
        real_image_code_text = real_image_code_text.decode()
        if real_image_code_text.lower() != text.lower():
            raise serializers.ValidationError('图片验证码错误')

        # 判断是否在60s内
        # get_serializer 方法在创建序列化器对象的时候，会补充context属性
        # context 属性中包含三个值：request  format  view 类视图对象
        # django 的类视图对象中，kwargs属性保存了路径提取出来的参数
        # mobile = self.context['view'].kwargs['mobile']

        send_flag = redis_conn.get("send_flag_%s" % mobile)
        if send_flag:
            raise serializers.ValidationError('请求次数过于频繁')

        return attrs