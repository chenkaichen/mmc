from rest_framework_jwt.authentication import JSONWebTokenAuthentication


""" 自定义认证类，在用户没有登录的时候，返回None """
class MyJSONWebTokenAuthentication(JSONWebTokenAuthentication):
    def authenticate(self, request):
        try:
            return super().authenticate(request)
        except:
            return None