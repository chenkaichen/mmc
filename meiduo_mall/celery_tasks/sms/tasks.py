from rest_framework import status
from rest_framework.response import Response
from celery_tasks.sms.utils.yuntongxun import sms
from verifications import constants
from celery_tasks.main import celery_app
import logging


logger = logging.getLogger('django')


@celery_app.task(name='send_sms_code')
def send_sms_code(mobile, sms_code):
    """发送短信验证码"""
    try:
        # expires = str(constants.SMS_CODE_REDIS_EXPIRES // 60)
        # ccp = sms.CCP()
        # result = ccp.send_template_sms(mobile, [sms_code, expires], constants.SMS_TEMPLATE_ID)

        result = 0  # 默认发送成功
    except Exception as e:
        # 发送短信异常
        logger.error(f"发送短信验证码异常:[mobile: {mobile}, message: {e}]")

    # 如果发送短信没有异常，则获取发送短信的状态
    else:
        if result == 0:
            logger.info(f"发送短信验证码成功:[mobile: {mobile}]")
        else:
            logger.info(f"发送短信验证码失败:[mobile: {mobile}]")